const cards = document.querySelectorAll(".memory-card")

let hasFlippedCard = false;
let lockBoard = true;
let firstCard, secondCard;
let matches = 0;
let time = 30;

let timerStart = document.getElementById("start");
let timerReset = document.getElementById("reset");

function sound(src) { // <- will serve as the template or constructor for sounds to be used
  	this.sound = document.createElement("audio");
	this.sound.src = src;
	this.sound.setAttribute("preload", "auto");
	this.sound.setAttribute("controls", "none");
	this.sound.style.display = "none";
	document.body.appendChild(this.sound);
	this.sound.loop = false;

	this.play = function() {
    	this.sound.play();
	}

  	this.stop = function() {
    	this.sound.pause();
 	}
}



timerStart.addEventListener("click", function () { // <- starts the game and timer
	
	let startSound = new sound("./assets/sounds/start.mp3");
	console.log(startSound.play());		
	document.getElementById("message").innerHTML = "Start!"
	this.disabled = true;
	this.classList.add("disabled");
	lockBoard = false;

	let timer = setInterval(function countTime () {
		if(time == 0){
			let loseSound = new sound("./assets/sounds/lose.mp3")
			loseSound.play();

			lockBoard = true;
			document.getElementById("timerDisplay").innerHTML = "TIME'S UP!!!";
			document.getElementById("message").innerHTML = "Try again?"

			clearInterval(timer); /*Stop timer*/
			return;
		} else if(matches == 6) {	
			let winSound = new sound("./assets/sounds/win.mp3");
			winSound.play()		

			if(time >= 20){
				document.getElementById("message").innerHTML = "Congrats you're pretty fast!!"				
			} else if(time >= 6) {
				document.getElementById("message").innerHTML = "Not bad, I bet you can do better"
			} else if(time >= 1) {
				document.getElementById("message").innerHTML = "Clutch! but maybe try clicking faster"
			}

			clearInterval(timer); /*Stop timer*/
		} else if(time != 0){
			time--;
			document.getElementById("timerDisplay").innerHTML = time + " seconds remaining";
		} 
	}, 1000)	
});

timerReset.addEventListener("click", function () { // <- resets/refrehses the page
	location.reload();
});

function flipCard() {
	if (lockBoard) return;
	if (this === firstCard) return;

	this.classList.add("flip");

	if (!hasFlippedCard) {
		//first click
		hasFlippedCard = true;
		firstCard = this;
	} else {
		//second click
		hasFlippedCard = false; //let it go back to false
		secondCard = this;

		checkForMatch();
	}
}

function checkForMatch () {
	//do cards match?
	if (firstCard.dataset.name === secondCard.dataset.name) {
		let matchSound = new sound("./assets/sounds/match.mp3")
		matchSound.play();

		document.getElementById("matches").innerHTML = (matches += 1) + " out of 6"; // <- +1 to matches
		disableCards(); // <- its a match
	} else {
		unflipCards(); // <- not a match
	}
}

function disableCards() {
	firstCard.removeEventListener('click', flipCard);
	secondCard.removeEventListener('click', flipCard);

	resetBoard();
}

function unflipCards() {
	lockBoard = true;

	setTimeout(() => {
		firstCard.classList.remove("flip");
		secondCard.classList.remove("flip");

	resetBoard();
	}, 500);
}

function resetBoard() {
	[hasFlippedCard, lockBoard] = [false, false];
	[firstCard, secondCard] = [null, null];
}

(function shuffle() {
	cards.forEach(card => {
		let randomPos = Math.floor(Math.random() * 12);
		card.style.order = randomPos;
	})
})();

cards.forEach(card => card.addEventListener("click", flipCard))